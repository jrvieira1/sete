# Sete

hexagonal cellular automata

[![asciicast](https://asciinema.org/a/531791.svg)](https://asciinema.org/a/531791)

## try

### nice pattern

running Dois from random noise, let it stabilize then switch to Terra


## License
This project is licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/).